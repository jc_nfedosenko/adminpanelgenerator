import {Component, Input, OnInit} from '@angular/core';

import {DataStorageJSON} from '../../services/DataStorageJSON';
import {ListingData} from "../../models/ListingData";

@Component({
  selector: 'listing',
  templateUrl: 'listing.component.html',
  styleUrls: ['listing.component.css']
})
export class ListingComponent implements OnInit {
  @Input() path: string;
  errorMessage: string;
  data: ListingData;

  constructor(private dataStorageService: DataStorageJSON) {}

  ngOnInit() {
    this.getData();
  }


  getData() {
    this.dataStorageService.get(this.path)
      .subscribe(
        data => this.data = data,
        error => this.errorMessage = <any>error
      );
  }

}
