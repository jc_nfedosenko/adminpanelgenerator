import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  layout = `
<div class="container">
  <p class="lead text-center">Dynamic content</p>
  <div class="col-md-6 col-md-offset-3">
    <listing [path]="'../assets/listing.json'"></listing>
  </div>
  <div class="col-md-3">
    <menu></menu>
  </div>
</div>`;
}
