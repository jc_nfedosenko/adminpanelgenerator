import {Component, ComponentRef, ViewChild, ViewContainerRef, Input}   from '@angular/core';
import {ComponentFactory, AfterViewInit}          from '@angular/core';

import {DynamicTypeBuilder} from '../../services/DynamicTypeBuilderService';

@Component({
  selector: 'dynamic-detail',
  template: `<div #dynamicContentPlaceHolder></div>`,
})
export class DynamicDetail implements AfterViewInit {
  @Input() layout: string;

  // reference for a <div> with #dynamicContentPlaceHolder
  @ViewChild('dynamicContentPlaceHolder', {read: ViewContainerRef})
  protected dynamicComponentTarget: ViewContainerRef;

  // this will be reference to dynamic content - to be able to destroy it
  protected componentRef: ComponentRef<any>;

  // wee need Dynamic component builder
  constructor(protected typeBuilder: DynamicTypeBuilder) {
  }

  /** Get a Factory and create a component */

  protected refreshContent() {

    if (this.componentRef) {
      this.componentRef.destroy();
    }

    // here we get Factory (just compiled or from cache)
    this.typeBuilder
      .createComponentFactory(this.layout)
      .then((factory: ComponentFactory<any>) => {
        this.componentRef = this
          .dynamicComponentTarget
          .createComponent(factory);
      });
  }

  public ngAfterViewInit(): void {
    this.refreshContent();
  }
}
