import {Observable} from "rxjs";
export interface IDataStorage {
  get(path: string): Observable<any>;
}
