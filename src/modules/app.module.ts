import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {COMPILER_PROVIDERS} from '@angular/compiler';

import {AppComponent} from '../components/app/app.component';

import {DynamicModule}    from './dynamic.module';

import {DataStorageJSON} from '../services/DataStorageJSON';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DynamicModule.forRoot()
  ],
  providers: [
    DataStorageJSON,
    COMPILER_PROVIDERS
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
