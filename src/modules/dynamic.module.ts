import {NgModule}      from '@angular/core';

import {PartsModule}   from './partials.module';

import {DynamicDetail}          from '../components/dynamic/dynamic.component';
import {DynamicTypeBuilder}     from '../services/DynamicTypeBuilderService';

@NgModule({
  imports: [PartsModule],
  declarations: [DynamicDetail],
  exports: [DynamicDetail],
})
export class DynamicModule {

  static forRoot() {
    return {
      ngModule: DynamicModule,
      providers: [
        DynamicTypeBuilder
      ],
    };
  }
}
