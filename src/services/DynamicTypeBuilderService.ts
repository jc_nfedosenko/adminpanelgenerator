import {Component, ComponentFactory, NgModule, Input, Injectable} from '@angular/core';
import {JitCompiler} from '@angular/compiler';
import {PartsModule} from '../modules/partials.module';

import * as _ from 'underscore';


@Injectable()
export class DynamicTypeBuilder {

  // wee need Dynamic component builder
  constructor(protected compiler: JitCompiler) {
  }

  // this object is singleton - so we can use this as a cache
  private _cacheOfFactories: {[templateKey: string]: ComponentFactory<any>} = {};

  public createComponentFactory(template: string): Promise<ComponentFactory<any>> {

    let factory = this._cacheOfFactories[template];

    if (factory) {
      console.log("Module and Type are returned from cache");

      return new Promise((resolve) => {
        resolve(factory);
      });
    }

    let type = DynamicTypeBuilder.createNewComponent(template);
    let module = DynamicTypeBuilder.createComponentModule(type);

    return new Promise((resolve) => {
      this.compiler
        .compileModuleAndAllComponentsAsync(module)
        .then((moduleWithFactories) => {
          factory = _.find(moduleWithFactories.componentFactories, {componentType: type});

          this._cacheOfFactories[template] = factory;

          resolve(factory);
        });
    });
  }

  protected static createNewComponent(tmpl: string) {
    @Component({
      selector: 'dynamic-component',
      template: tmpl,
    })
    class CustomDynamicComponent {
    }

    return CustomDynamicComponent;
  }

  protected static createComponentModule(componentType: any) {
    @NgModule({
      imports: [
        PartsModule,
      ],
      declarations: [
        componentType
      ],
    })
    class RuntimeComponentModule {
    }
    // a module for just this Type
    return RuntimeComponentModule;
  }
}
