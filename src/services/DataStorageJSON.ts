import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';

import {Observable} from "rxjs";
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import {IDataStorage} from '../interfaces/IDataStorage';
import {ListingData} from '../models/ListingData';

@Injectable()
export class DataStorageJSON implements IDataStorage {

  constructor(private http: Http) {}

  public get(path: string): Observable<ListingData> {
    return this.http.get(path)
      .map(DataStorageJSON.extractData)
      .catch(DataStorageJSON.handleError);
  }

  private static extractData(res: Response) {
    let body = res.json();
    return body.data || {};
  }

  private static handleError(error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}
