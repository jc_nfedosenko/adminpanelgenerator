import { AdminPanelGeneratorPage } from './app.po';

describe('admin-panel-generator App', () => {
  let page: AdminPanelGeneratorPage;

  beforeEach(() => {
    page = new AdminPanelGeneratorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
